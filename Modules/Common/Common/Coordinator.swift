//
//  Coordinator.swift
//  Apps
//
//  Created by Ihwan on 26/07/21.
//

import Foundation
import UIKit

public protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}
