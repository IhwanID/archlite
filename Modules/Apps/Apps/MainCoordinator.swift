//
//  MainCoordinator.swift
//  Apps
//
//  Created by Ihwan on 26/07/21.
//

import Foundation
import UIKit
import FeatureA
import Common

public class MainCoordinator: Coordinator {
    public var childCoordinators = [Coordinator]()
    public var navigationController: UINavigationController

        public init(navigationController: UINavigationController) {
            self.navigationController = navigationController
        }


    public func start() {
        let vc = FeatureAViewController() //UIStoryboard(name: "FeatureA", bundle: Bundle(identifier: "id.ihwan.FeatureA")).instantiateViewController(withIdentifier: "FeatureAViewController") as! FeatureAViewController
        vc.coordinator = self
        navigationController.setViewControllers([vc], animated: false)    }
}
